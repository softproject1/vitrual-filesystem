package pl.com.softproject.filesystem

import pl.com.softproject.filesystem.files.Directory

/**
  * @author Adrian Lapierre { @literal <alapierre@soft-project.pl>}
  */
class State(val root: Directory, val wd: Directory, output: String) {

  def show(): State = {
    println(output)
    print(State.SHELL_TOKEN)
    this
  }

  def setMessage(message: String) : State = State(root, wd, message)
}

object State {
  val SHELL_TOKEN = "$ "

  def apply(root: Directory, wd: Directory, output: String = ""): State =
    new State(root, wd, output)

}