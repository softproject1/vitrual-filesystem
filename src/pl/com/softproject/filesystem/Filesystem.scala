package pl.com.softproject.filesystem

import java.util.Scanner

import pl.com.softproject.filesystem.commands.Command
import pl.com.softproject.filesystem.files.Directory

/**
  * @author Adrian Lapierre { @literal <alapierre@soft-project.pl>}
  */
object Filesystem extends App {

  val root = Directory.ROOT
//  var state = State(root, root)
//  val scanner = new Scanner(System.in)
//
//  while (true) {
//    state.show()
//    val input = scanner.nextLine()
//    state = Command.from(input).apply(state)
//  }

  io.Source.stdin.getLines().foldLeft(State(root, root).show())((currentState, newLine) => {
    Command.from(newLine).
      apply(currentState).
      show()
  })

}
