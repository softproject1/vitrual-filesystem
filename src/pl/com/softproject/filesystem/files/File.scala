package pl.com.softproject.filesystem.files

import pl.com.softproject.filesystem.FilesystemException

/**
  * @author Adrian Lapierre { @literal <alapierre@soft-project.pl>}
  */
class File(override val parentPath: String, override val name: String, content: String)
  extends DirEntry(parentPath, name) {

  override def asDirectory: Directory = throw new FilesystemException("File canot be converted to directory")

  override def getType: String = "File"

  override def asFile: File = this

  override def isDirectory: Boolean = false

  override def isFile: Boolean = true
}

object File {

  def empty(parentPath: String, name: String) = new File(parentPath, name, "")
}