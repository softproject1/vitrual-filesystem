package pl.com.softproject.filesystem.files

/**
  * @author Adrian Lapierre { @literal <alapierre@soft-project.pl>}
  */
abstract class DirEntry(val parentPath: String, val name: String) {

  def path: String = {
    val separatorIfNessesary =
      if(Directory.ROOT_PATH.equals(parentPath)) ""
      else Directory.SEPARATOR

    parentPath + separatorIfNessesary + name
  }

  def asDirectory: Directory
  def asFile: File
  def getType: String
  def isDirectory: Boolean
  def isFile: Boolean
}
