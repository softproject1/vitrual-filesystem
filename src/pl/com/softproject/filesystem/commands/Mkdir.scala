package pl.com.softproject.filesystem.commands
import pl.com.softproject.filesystem.State
import pl.com.softproject.filesystem.files.{DirEntry, Directory}

/**
  * @author Adrian Lapierre { @literal <alapierre@soft-project.pl>}
  */
class Mkdir(name: String) extends CreateEntry(name) {
  override def createSpecificEntry(state: State): DirEntry = Directory.empty(state.wd.path, name)
}
