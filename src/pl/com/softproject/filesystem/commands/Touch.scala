package pl.com.softproject.filesystem.commands
import pl.com.softproject.filesystem.State
import pl.com.softproject.filesystem.files.{DirEntry, File}

/**
  * @author Adrian Lapierre { @literal <alapierre@soft-project.pl>}
  */
class Touch(name: String) extends CreateEntry(name) {
  override def createSpecificEntry(state: State): DirEntry = File.empty(state.wd.path, name)
}
