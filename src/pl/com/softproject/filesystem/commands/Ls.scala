package pl.com.softproject.filesystem.commands
import pl.com.softproject.filesystem.State
import pl.com.softproject.filesystem.files.DirEntry

/**
  * @author Adrian Lapierre { @literal <alapierre@soft-project.pl>}
  */
class Ls extends Command {

  def createNiceOutput(contents: List[DirEntry]) : String = {
    if(contents.isEmpty) ""
    else {
      val entry = contents.head
      entry.name + "[" + entry.getType + "]\n" + createNiceOutput(contents.tail)
    }
  }

  override def apply(state: State): State = {
    val contents = state.wd.contents
    val niceOutput = createNiceOutput(contents)
    state.setMessage(niceOutput)
  }
}
