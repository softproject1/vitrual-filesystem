package pl.com.softproject.filesystem.commands
import pl.com.softproject.filesystem.State

/**
  * @author Adrian Lapierre { @literal <alapierre@soft-project.pl>}
  */
class UnknownCommand extends Command {
  override def apply(state: State): State = state.setMessage("Command not found")
}
