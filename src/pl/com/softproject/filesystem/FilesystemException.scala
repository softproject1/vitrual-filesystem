package pl.com.softproject.filesystem

/**
  * @author Adrian Lapierre { @literal <alapierre@soft-project.pl>}
  */
class FilesystemException(message: String) extends RuntimeException(message) {

}
